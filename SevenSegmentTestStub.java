
public class SevenSegmentTestStub implements SevenSegment {


	
	private boolean [] array;
	
	public SevenSegmentTestStub() {
		
		array = new boolean [7];
	}
	@Override
	public void setLED(int led, boolean on) {
		array[led] = on;
	}
	
	public boolean [] getArray() {
		return array;
	}
}
