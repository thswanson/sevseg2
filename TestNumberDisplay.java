import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.Assert.*;


public class TestNumberDisplay {

	
	NumberDisplay nd;
	SevenSegmentTestStub ssts;
	@Before
	public void setup() {
		ssts = new SevenSegmentTestStub();
		nd = new NumberDisplayImpl(ssts);
	}
	
	@Test
	public void testLeds0() {
		nd.display(0);
		boolean [] array = ssts.getArray();
		assertTrue(array[0]);
		assertTrue(array[1]);
		assertTrue(array[2]);
		assertTrue(array[4]);
		assertTrue(array[5]);
		assertTrue(array[6]);
	}
	
	@Test
	public void testLeds1() {
		nd.display(1);
		boolean [] array = ssts.getArray();	
		assertTrue(array[2]);
		assertTrue(array[5]);
	}
	@Test
	public void testLeds2() {
		nd.display(2);
		boolean [] array = ssts.getArray();	
		assertTrue(array[0]);
		assertTrue(array[2]);
		assertTrue(array[3]);
		assertTrue(array[4]);
		
		assertTrue(array[6]);
	}
	@Test
	public void testLeds3() {
		nd.display(3);
		boolean [] array = ssts.getArray();	
		assertTrue(array[0]);
		assertTrue(array[2]);
		assertTrue(array[3]);
		assertTrue(array[5]);
		
		assertTrue(array[6]);
	}
	@Test
	public void testLeds4() {
		nd.display(4);
		boolean [] array = ssts.getArray();	
		assertTrue(array[1]);
		assertTrue(array[2]);
		assertTrue(array[3]);
		assertTrue(array[5]);
	}
	@Test
	public void testLeds5() {
		nd.display(5);
		boolean [] array = ssts.getArray();	
		assertTrue(array[1]);
		assertTrue(array[0]);
		assertTrue(array[3]);
		assertTrue(array[5]);
		assertTrue(array[6]);
	}
	@Test
	public void testLeds6() {
		nd.display(6);
		boolean [] array = ssts.getArray();	
		assertTrue(array[0]);
		assertTrue(array[1]);
		assertTrue(array[4]);
		assertTrue(array[3]);
		assertTrue(array[5]);
		assertTrue(array[6]);
	}
	@Test
	public void testLeds7() {
		nd.display(7);
		boolean [] array = ssts.getArray();	
		assertTrue(array[0]);
		assertTrue(array[2]);
		assertTrue(array[5]);
	}
	@Test
	public void testLeds8() {
		nd.display(8);
		boolean [] array = ssts.getArray();
		assertTrue(array[0]);
		assertTrue(array[1]);
		assertTrue(array[2]);
		assertTrue(array[3]);
		assertTrue(array[4]);
		assertTrue(array[5]);
		assertTrue(array[6]);
	}
	@Test
	public void testLeds9() {
		nd.display(8);
		boolean [] array = ssts.getArray();
		assertTrue(array[0]);
		assertTrue(array[1]);
		assertTrue(array[2]);
		assertTrue(array[3]);
		assertTrue(array[5]);
	}
}
