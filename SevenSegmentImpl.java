
public class SevenSegmentImpl implements SevenSegment {

	
	private boolean led0;
	private boolean led1;
	private boolean led2;
	private boolean led3;
	private boolean led4;
	private boolean led5;
	private boolean led6;
	
	public SevenSegmentImpl() {
		led0 = false;
		led1= false;
		led2= false;
		led3= false;
		led4= false;
		led5= false;
		led6= false;
	}
	@Override
	public void setLED(int led, boolean on) {
		switch(led) {
			case 0:
				led0 = on;
			case 1:
				led1 = on;
			case 2:
				led2 = on;
			case 3:
				led3 = on;
			case 4: 
				led4 = on;
			case 5:
				led5 = on;
			case 6:
				led6 = on;
		}
	}
	
	public void setNumber(int number) {
		setLedOff();
		switch(number) {
			case 0:
				setLED(0,true);
				setLED(1,true);
				setLED(2,true);
				setLED(4,true);
				setLED(5,true);
				setLED(6,true);
			case 1:
				setLED(2,true);
				setLED(4,true);
			case 2:
				setLED(0,true);
				setLED(2,true);
				setLED(3, true);
				setLED(4,true);
				setLED(6,true);
			case 3:
				setLED(0,true);
				setLED(2,true);
				setLED(3, true);
				setLED(5,true);
				setLED(6,true);
			case 4: 
				setLED(1,true);
				setLED(2,true);
				setLED(3,true);
				setLED(5,true);
			case 5:
				setLED(0,true);
				setLED(1,true);
				setLED(3, true);
				setLED(5,true);
				setLED(6,true);
			case 6:
				setLED(0,true);
				setLED(1,true);
				setLED(3, true);
				setLED(5,true);
				setLED(4, true);
				setLED(6,true);
			case 7:
				setLED(0,true);
				setLED(2,true);
				setLED(3, true);
				setLED(5, true);
			case 8:
				setLED(0,true);
				setLED(1,true);
				setLED(2,true);
				setLED(3,true);
				setLED(4,true);
				setLED(5,true);
				setLED(6,true);
		}
	}
	public void setLedOff() {
		led0 = false;
		led1= false;
		led2= false;
		led3= false;
		led4= false;
		led5= false;
		led6= false;
	}
	
}
