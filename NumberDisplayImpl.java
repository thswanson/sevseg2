
public class NumberDisplayImpl implements NumberDisplay {

	
	SevenSegment ss;
	
	public NumberDisplayImpl (SevenSegment ss) {
		this.ss = ss;
	}
	@Override
	public void display(int number) {
		//setLedOff();
		switch(number) {
			case 0:
				ss.setLED(0,true);
				ss.setLED(1,true);
				ss.setLED(2,true);
				ss.setLED(4,true);
				ss.setLED(5,true);
				ss.setLED(6,true);
				break;
			case 1:
				ss.setLED(2,true);
				ss.setLED(5,true);
				break;
			case 2:
				ss.setLED(0,true);
				ss.setLED(2,true);
				ss.setLED(3, true);
				ss.setLED(4,true);
				ss.setLED(6,true);
				break;
			case 3:
				ss.setLED(0,true);
				ss.setLED(2,true);
				ss.setLED(3, true);
				ss.setLED(5,true);
				ss.setLED(6,true);
				break;
			case 4: 
				ss.setLED(1,true);
				ss.setLED(2,true);
				ss.setLED(3,true);
				ss.setLED(5,true);
				break;
			case 5:
				ss.setLED(0,true);
				ss.setLED(1,true);
				ss.setLED(3, true);
				ss.setLED(5,true);
				ss.setLED(6,true);
				break;
			case 6:
				ss.setLED(0,true);
				ss.setLED(1,true);
				ss.setLED(3, true);
				ss.setLED(5,true);
				ss.setLED(4, true);
				ss.setLED(6,true);
				break;
			case 7:
				ss.setLED(0,true);
				ss.setLED(2,true);
				ss.setLED(3, true);
				ss.setLED(5, true);
				break;
			case 8:
				ss.setLED(0,true);
				ss.setLED(1,true);
				ss.setLED(2,true);
				ss.setLED(3,true);
				ss.setLED(4,true);
				ss.setLED(5,true);
				ss.setLED(6,true);
				break;
		}

	}

}
